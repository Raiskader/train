﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise4
{
    public enum Direction
    {
        West,
        North,
        South,
        East
    }

    public enum Track
    {
        track1,
        track2
    }

    public enum typetrain
    {
        loco,
        wagon,
        secondary,
        tertiary
    }

    public enum Colortype
    {
        blue,
        red,
        green,
        purple,
        orange,
        yellow
    }
    public class PanelThread
    {
        private Point origin;
        private int delay;
        private Panel panel;
        public Direction direction;
        public Track track;
        public typetrain locwag;
        public Colortype coltype;
        private Color colour;
        private Point plane;
        public int xDelta;
        public int yDelta;
        private Semaphore semaphore;
        public bool greenlaunched = false;
        public bool purplelaunched = false;
        public bool orangelaunched = false;
        public bool yellowlaunched = false;
        public bool blackclicked = false;
        public bool blackclicked2 = false;
        public bool greenclicked = false;
        public bool purpleclicked = false;
        public bool orangeclicked = false;
        public bool yellowclicked = false;
        public bool waitingblack = false;
        public bool waitingblack2 = false;
        public bool waitinggreen = false;
        public bool waitingpurple = false;
        public bool waitingorange = false;
        public bool waitingyellow = false;
        public bool ontrack = true;



        public PanelThread(Point origin,
                           int delay,
                           Direction direction,
                           Track track,
                           Panel panel,
                           typetrain locwag,
                           Color colour,
                           Colortype coltype,
                           Semaphore semaphore)
        {
            this.origin = origin;
            this.delay = delay;
            this.direction = direction;
            this.panel = panel;
            this.colour = colour;
            this.plane = origin;
            this.panel.Paint += new PaintEventHandler(this.panel_Paint);
            this.track = track;
            this.locwag = locwag;
            switch (this.direction)
            {
                case Direction.East:
                    this.xDelta = -10;
                    this.yDelta = 0;
                    break;

                case Direction.West:
                    this.xDelta = +10;
                    this.yDelta = 0;
                    break;

                case Direction.North:
                    this.xDelta = 0;
                    this.yDelta = -10;
                    break;

                case Direction.South:
                    this.xDelta = 0;
                    this.yDelta = +10;
                    break;
            }
            
            
            this.semaphore = semaphore;
        }



        public void Start()
        {
            Color signal = Color.Red;
            Thread.Sleep(delay);
            for (;;)
            {
                if (this.locwag == typetrain.loco)
                {
                    //ontrack = true;
                    work();
                }
                else
                    for (;;)
                    {
                        if (this.locwag == typetrain.wagon)
                        {
                            for (;;)
                            {
                                if (greenclicked == true)
                                {
                                    this.colour = Color.Green;
                                    workwagon();
                                    greenclicked = false;
                                }
                                if (purpleclicked == true)
                                {
                                    this.colour = Color.Purple;
                                    workwagon();
                                    purpleclicked = false;
                                }
                                if (orangeclicked == true)
                                {
                                    this.colour = Color.OrangeRed;
                                    workwagon();
                                    orangeclicked = false;
                                }
                                if (yellowclicked == true)
                                {
                                    this.colour = Color.Yellow;
                                    workwagon();
                                    yellowclicked = false;
                                }
                            }
                        }
                        if (this.locwag == typetrain.secondary || this.locwag == typetrain.tertiary || this.locwag == typetrain.loco)
                        {
                            for (;;)
                            {
                                if (blackclicked == true || blackclicked2 == true)
                                {
                                    if (this.coltype == Colortype.yellow)
                                    {
                                        colour = Color.Yellow;
                                    }
                                    if (this.coltype == Colortype.orange)
                                    {
                                        colour = Color.OrangeRed;
                                    }
                                    if (this.coltype == Colortype.purple)
                                    {
                                        colour = Color.Purple;
                                    }
                                    if (this.coltype == Colortype.green)
                                    {
                                        colour = Color.Green;
                                    }
                                    if (this.coltype == Colortype.red)
                                    {
                                        colour = Color.Red;
                                    }
                                    if (this.coltype == Colortype.blue)
                                    {
                                        colour = Color.Blue;
                                    }
                                    blackclicked = false;
                                    blackclicked2 = false;
                                    work();
                                    break;
                                }
                            }
                        }
                    }
            }
        }

        public void work()
        {
            int start = 1;
            if(this.locwag == typetrain.secondary)
            {
                start = 3;
            }
            if(this.track == Track.track1)
            {
                for (int k = start; k <= 8; k++)
                {
                    for (int i = 1; i <= 26; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    for (int i = 1; i <= 26; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    this.changeDirection(xDelta, yDelta);
                    switch (k)
                    {

                        case 2:
                            for (int e = 0; e < 10; e++)
                            {
                                waitinggreen = true;
                                Thread.Sleep(delay);
                                waitinggreen = false;
                                if (blackclicked2 == true)
                                {
                                    waitingblack2 = true;
                                    k = 8;
                                }
                            }
                            break;
                        case 3:
                            for (int e = 0; e < 10; e++)
                            {
                                waitingpurple = true;
                                Thread.Sleep(delay);
                                waitingpurple = false;
                            }
                            break;
                        case 4:
                            if(blackclicked == true)
                            {
                                waitingblack = true;
                                k = 8;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            break;

                    }
                }
                this.colour = Color.Red;
                panel.Invalidate();
            }
            else if (this.track == Track.track2)
            {
                for (int k = start; k <= 8; k++)
                {
                    for (int i = 1; i <= 19; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    for (int i = 1; i <= 19; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    this.changeDirection(xDelta, yDelta);
                    //semaphore.Signal();
                    switch (k)
                    {

                        case 1:
                            for (int e = 0; e < 10; e++)
                            {
                                waitingorange = true;
                                Thread.Sleep(delay);
                                waitingorange = false;
                            }
                            break;
                        case 2:
                            if (blackclicked2 == true)
                            {
                                waitingblack2 = true;
                                k = 8;
                            }
                            break;
                        case 3:
                            for (int e = 0; e < 10; e++)
                            {
                                waitingyellow = true;
                                Thread.Sleep(delay);
                                waitingyellow = true;
                            }
                            break;
                        case 4:
                            if (blackclicked == true)
                            {
                                waitingblack = true;
                                k = 8;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            break;

                    }
                }
                this.colour = Color.Blue;
                panel.Invalidate();
            }
        }

        public void workwagon()
        {
            if (this.track == Track.track1)
            {
                int start = 0;
                if(greenclicked == true)
                {
                    start = 3;
                    for (int e = 0; e < 12; e++)
                    {
                        Thread.Sleep(delay);

                    }
                }
                if (purpleclicked == true)
                {
                    start = 4;
                    for (int e = 0; e < 14; e++)
                    {
                        Thread.Sleep(delay);

                    }
                }
                for (int k = start; k <= 8; k++)
                {
                    for (int i = 1; i <= 26; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    for (int i = 1; i <= 26; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    this.changeDirection(xDelta, yDelta);
                    switch (k)
                    {

                        case 2:
                            for (int e = 0; e < 10; e++)
                            {
                                Thread.Sleep(delay);
                                if (blackclicked2 == true)
                                {
                                    waitingblack2 = true;
                                    k = 8;
                                }

                            }
                            break;
                        case 3:
                            for (int e = 0; e < 10; e++)
                            {
                                Thread.Sleep(delay);
                            }
                            break;
                        case 4:
                            if (blackclicked == true)
                            {
                                waitingblack = true;
                                k = 8;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            break;

                    }
                }
                this.colour = Color.DodgerBlue;
                panel.Invalidate();
            }
            else if (this.track == Track.track2)
            {
                int start = 0;
                if (orangeclicked == true)
                {
                    start = 2;
                    for (int e = 0; e < 16; e++)
                    {
                        Thread.Sleep(delay);

                    }
                }
                if (yellowclicked == true)
                {
                    start = 4;
                    for (int e = 0; e < 18; e++)
                    {
                        Thread.Sleep(delay);

                    }
                }
                for (int k = start; k <= 8; k++)
                {
                    for (int i = 1; i <= 19; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    for (int i = 1; i <= 19; i++)
                    {
                        this.movePlane(xDelta, yDelta);
                        Thread.Sleep(delay);
                        panel.Invalidate();
                    }
                    this.changeDirection(xDelta, yDelta);
                    //semaphore.Signal();
                    switch (k)
                    {

                        case 1:
                            for (int e = 0; e < 10; e++)
                            {
                                Thread.Sleep(delay);
                            }
                            break;
                        case 2:
                            {
                                if (blackclicked2 == true)
                                {
                                    waitingblack2 = true;
                                    k = 8;
                                }
                            }
                            break;
                        case 3:
                            for (int e = 0; e < 10; e++)
                            {
                                Thread.Sleep(delay);
                            }
                            break;
                        case 4:
                            if (blackclicked == true)
                            {
                                waitingblack = true;
                                k = 8;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            break;

                    }
                }
                this.colour = Color.Silver;
                panel.Invalidate();
            }
        }
        private void zeroPlane()
        {
            plane.X = origin.X;
            plane.Y = origin.Y;
        }

        private void movePlane(int xDelta, int yDelta)
        {
            plane.X += xDelta; plane.Y += yDelta;
        }

        public void changeDirection(int xDelta, int yDelta)
        {
            if(this.xDelta == +10)
            {
                this.yDelta = +10;
                this.xDelta = 0;
            }
            else if (this.xDelta == -10)
            {
                this.yDelta = -10;
                this.xDelta = 0;
            }
            else if (this.yDelta == +10)
            {
                this.xDelta = -10;
                this.yDelta = 0;
            }
            else if (this.yDelta == -10)
            {
                this.xDelta = +10;
                this.yDelta = 0;
            }
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            SolidBrush brush = new SolidBrush(colour);
            g.FillRectangle(brush, plane.X, plane.Y, 10, 10);
            brush.Dispose();    //  Dispose of graphic
            //  resources  
        }

        public void waitingwagon()
        {
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            while (!autoResetEvent.WaitOne(TimeSpan.FromSeconds(0.1)))
            {
                if (this.track == Track.track1)
                {
                    if (blackclicked == true || blackclicked2 == true)
                    {
                        if (waitingblack == true || waitingblack2 == true)
                        {
                            waitingblack = false;
                            waitingblack2 = false;
                            break;
                        }
                    }
                    else if (purpleclicked == true)
                    {
                        if (waitingpurple == true)
                        {
                            break;
                        }
                    }
                    else if (greenclicked == true)
                    {
                        if (waitinggreen == true)
                        {
                            break;
                        }
                    }
                }
                else if(this.track == Track.track2)
                {
                    if (blackclicked == true || blackclicked2 == true)
                    {
                        if (waitingblack == true || waitingblack2 == true)
                        {
                            waitingblack = false;
                            break;
                        }
                    }
                    else if (yellowclicked == true)
                    {
                        if (waitingyellow == true)
                        {
                            break;
                        }
                    }
                    if (orangeclicked == true)
                    {
                        if (waitingorange == true)
                        {
                            break;
                        }
                    }
                }
            }
        }

        public void wait()
        {
            for (int e = 0; e < 2; e++)
            {
                Thread.Sleep(delay);
            }
        }
    }// end class PanelThread
}
