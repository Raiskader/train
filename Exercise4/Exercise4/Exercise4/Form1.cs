﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise4
{
    public partial class Form1 : Form
    {
        private Container components = null;
        public PanelThread p1t1, p1t2, p2t2, p2t1, green1t1, green1t2, green2t1, green2t2, purple1t1, purple1t2, purple2t1, purple2t2,orange1t1, orange1t2, orange2t1, orange2t2, yellow1t1, yellow1t2, yellow2t1, yellow2t2;
        private Thread thread1, thread2, thread3, thread4, thread5, thread6, thread7, thread8, thread9, thread10, thread11, thread12, thread13, thread14, thread15, thread16, thread17, thread18, thread19, thread20;
        private Semaphore semaphore;
        private Thread semThread;
        public Panel pnl1;
        private Panel line;

        public Form1()
        {
            InitializeComponent();


            semaphore = new Semaphore();



            p1t1 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track1, pnl1, typetrain.loco,
                                 Color.Blue,
                                 Colortype.blue,
                                 semaphore
                                 );

            p1t2 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track2, panel5, typetrain.secondary,
                                 Color.Silver,
                                 Colortype.blue,
                                 semaphore
                                 );



            p2t2 = new PanelThread(new Point(390, 390),
                                 150, Direction.East, Track.track2, panel5, typetrain.loco,
                                 Color.Red,
                                 Colortype.red,
                                 semaphore
                                 );

            p2t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.secondary,
                                 Color.DodgerBlue,
                                 Colortype.red,
                                 semaphore
                                 );

            green1t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.wagon,
                                 Color.DodgerBlue,
                                 Colortype.green,
                                 semaphore
                                 );

            green1t2 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track2, panel5, typetrain.secondary,
                                 Color.Silver,
                                 Colortype.green,
                                 semaphore
                                 );

            green2t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.wagon,
                                 Color.DodgerBlue,
                                 Colortype.green,
                                 semaphore
                                 );

            green2t2 = new PanelThread(new Point(390, 390),
                                 150, Direction.West, Track.track2, panel5, typetrain.tertiary,
                                 Color.Silver,
                                 Colortype.green,
                                 semaphore
                                 );

            purple1t1 = new PanelThread(new Point(10, 530),
                                 150, Direction.North, Track.track1, pnl1, typetrain.wagon,
                                 Color.DodgerBlue,
                                 Colortype.purple,
                                 semaphore
                                 );

            purple1t2 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track2, panel5, typetrain.secondary,
                                 Color.Silver,
                                 Colortype.purple,
                                 semaphore
                                 );

            purple2t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.wagon,
                                 Color.DodgerBlue,
                                 Colortype.purple,
                                 semaphore
                                 );

            purple2t2 = new PanelThread(new Point(390, 390),
                                 150, Direction.East, Track.track2, panel5, typetrain.tertiary,
                                 Color.Silver,
                                 Colortype.purple,
                                 semaphore
                                 );

            orange1t1 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track1, pnl1, typetrain.tertiary,
                                 Color.DodgerBlue,
                                 Colortype.orange,
                                 semaphore
                                 );

            orange1t2 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track2, panel5, typetrain.wagon,
                                 Color.Silver,
                                 Colortype.orange,
                                 semaphore
                                 );

            orange2t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.secondary,
                                 Color.DodgerBlue,
                                 Colortype.orange,
                                 semaphore
                                 );

            orange2t2 = new PanelThread(new Point(10, 390),
                                 150, Direction.North, Track.track2, panel5, typetrain.wagon,
                                 Color.Silver,
                                 Colortype.orange,
                                 semaphore
                                 );

            yellow1t1 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track1, pnl1, typetrain.tertiary,
                                 Color.DodgerBlue,
                                 Colortype.yellow,
                                 semaphore
                                 );

            yellow1t2 = new PanelThread(new Point(10, 10),
                                 150, Direction.West, Track.track2, panel5, typetrain.wagon,
                                 Color.Silver,
                                 Colortype.yellow,
                                 semaphore
                                 );

            yellow2t1 = new PanelThread(new Point(530, 530),
                                 150, Direction.East, Track.track1, pnl1, typetrain.secondary,
                                 Color.DodgerBlue,
                                 Colortype.yellow,
                                 semaphore
                                 );

            yellow2t2 = new PanelThread(new Point(390, 10),
                                 150, Direction.South, Track.track2, panel5, typetrain.wagon,
                                 Color.Silver,
                                 Colortype.yellow,
                                 semaphore
                                 );


            semThread = new Thread(new ThreadStart(semaphore.Start));

            thread1 = new Thread(new ThreadStart(p1t1.Start));

            thread2 = new Thread(new ThreadStart(p2t2.Start));

            thread3 = new Thread(new ThreadStart(green1t1.Start));

            thread4 = new Thread(new ThreadStart(purple1t1.Start));

            thread5 = new Thread(new ThreadStart(orange2t2.Start));

            thread6 = new Thread(new ThreadStart(yellow2t2.Start));

            thread7 = new Thread(new ThreadStart(p1t2.Start));

            thread8 = new Thread(new ThreadStart(p2t1.Start));

            thread9 = new Thread(new ThreadStart(green1t2.Start));

            thread10 = new Thread(new ThreadStart(green2t1.Start));

            thread11 = new Thread(new ThreadStart(green2t2.Start));

            thread12 = new Thread(new ThreadStart(purple1t2.Start));

            thread13 = new Thread(new ThreadStart(purple2t1.Start));

            thread14 = new Thread(new ThreadStart(purple2t2.Start));

            thread15 = new Thread(new ThreadStart(orange1t1.Start));

            thread16 = new Thread(new ThreadStart(orange1t2.Start));

            thread17 = new Thread(new ThreadStart(orange2t1.Start));

            thread18 = new Thread(new ThreadStart(yellow1t1.Start));

            thread19 = new Thread(new ThreadStart(yellow1t2.Start));

            thread20 = new Thread(new ThreadStart(yellow2t1.Start));

            this.Closing += new CancelEventHandler(this.Form1_Closing);

            semThread.Start();
            thread1.Start();
            thread2.Start();



        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.pnl1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.line = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.pnl1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl1
            // 
            this.pnl1.BackColor = System.Drawing.Color.DodgerBlue;
            this.pnl1.Controls.Add(this.panel4);
            this.pnl1.Location = new System.Drawing.Point(146, 45);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(550, 550);
            this.pnl1.TabIndex = 0;
            this.pnl1.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl1_Paint_1);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.panel2);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(34, 34);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(479, 477);
            this.panel4.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(446, 404);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(35, 10);
            this.panel2.TabIndex = 9;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.Location = new System.Drawing.Point(0, 15);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(40, 10);
            this.panel6.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Location = new System.Drawing.Point(40, 15);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(407, 407);
            this.panel5.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Location = new System.Drawing.Point(26, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(354, 352);
            this.panel1.TabIndex = 8;
            // 
            // line
            // 
            this.line.BackColor = System.Drawing.Color.Black;
            this.line.Location = new System.Drawing.Point(633, 45);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(10, 95);
            this.line.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel8.Controls.Add(this.button5);
            this.panel8.Location = new System.Drawing.Point(146, 594);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(30, 50);
            this.panel8.TabIndex = 10;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Purple;
            this.button5.Location = new System.Drawing.Point(2, 24);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 23);
            this.button5.TabIndex = 16;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel9.Controls.Add(this.button7);
            this.panel9.Controls.Add(this.button6);
            this.panel9.Location = new System.Drawing.Point(695, 565);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(80, 30);
            this.panel9.TabIndex = 11;
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Green;
            this.button7.Location = new System.Drawing.Point(51, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(26, 23);
            this.button7.TabIndex = 17;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DodgerBlue;
            this.button6.Location = new System.Drawing.Point(2, 74);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(26, 23);
            this.button6.TabIndex = 16;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Silver;
            this.panel10.Controls.Add(this.button2);
            this.panel10.Location = new System.Drawing.Point(220, 500);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(30, 50);
            this.panel10.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.OrangeRed;
            this.button2.Location = new System.Drawing.Point(2, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 23);
            this.button2.TabIndex = 16;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Silver;
            this.panel11.Controls.Add(this.button3);
            this.panel11.Location = new System.Drawing.Point(626, 94);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(30, 30);
            this.panel11.TabIndex = 13;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Yellow;
            this.button3.Location = new System.Drawing.Point(2, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 23);
            this.button3.TabIndex = 16;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DimGray;
            this.panel12.Controls.Add(this.button4);
            this.panel12.Location = new System.Drawing.Point(597, 501);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(30, 50);
            this.panel12.TabIndex = 14;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(2, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 23);
            this.button4.TabIndex = 16;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel13.Controls.Add(this.button1);
            this.panel13.Location = new System.Drawing.Point(77, 45);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(70, 30);
            this.panel13.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MidnightBlue;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Black;
            this.button8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button8.Location = new System.Drawing.Point(80, 90);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(26, 23);
            this.button8.TabIndex = 16;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Black;
            this.button9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button9.Location = new System.Drawing.Point(715, 473);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(26, 23);
            this.button9.TabIndex = 17;
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(803, 761);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.line);
            this.Name = "Form1";
            this.Text = "La gare nison";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnl1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
            // Environment is a System class.
            // Kill off all threads on exit.
            Environment.Exit(Environment.ExitCode);
        }
        public Panel panel5;
        public Panel panel8;
        public Button button5;
        public Panel panel9;
        public Button button6;
        public Panel panel10;
        public Button button2;
        public Panel panel11;
        public Button button3;
        public Panel panel12;

        private void pnl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private Button button4;
        private Panel panel13;
        private Button button1;

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (p2t2.ontrack == true)
            {
                thread6.Start();
                p2t2.yellowclicked = true;
                p2t2.waitingwagon();
                p2t2.yellowclicked = false;
                p2t2.yellowlaunched = true;
                yellow2t2.yellowclicked = true;
            }
            else
            {
                thread19.Start();
                p1t2.yellowclicked = true;
                p1t2.waitingwagon();
                p1t2.yellowclicked = false;
                p1t2.yellowlaunched = true;
                yellow1t2.yellowclicked = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private Button button9;

        private void button9_Click(object sender, EventArgs e)
        {
            p1t1.ontrack = true;
            p2t2.ontrack = true;
            p1t2.blackclicked2 = true;
            p1t2.waitingwagon();
            p1t2.blackclicked2 = false;
            /*if (p1t1.greenlaunched == true)
            {
                green1t2.blackclicked2 = true;
                green1t2.waitingwagon();
                green1t2.blackclicked2 = false;
            }
            if (p1t1.purplelaunched == true)
            {
                purple1t2.blackclicked2 = true;
                purple1t2.waitingwagon();
                purple1t2.blackclicked2 = false;
            }
            if (p1t1.orangelaunched == true)
            {
                orange1t2.blackclicked2 = true;
                orange1t2.waitingwagon();
                orange1t2.blackclicked2 = false;
            }
            if (p1t1.yellowlaunched == true)
            {
                yellow1t2.blackclicked2 = true;
                yellow1t2.waitingwagon();
                yellow1t2.blackclicked2 = false;
            }*/

            thread7.Abort();
            thread1.Start();
            p1t1.blackclicked2 = true;
            /*if (p1t1.greenlaunched == true)
            {
                thread9.Abort();
                thread3.Start();
                green1t1.wait();
                green1t1.coltype = Colortype.green;
                green1t1.blackclicked2 = true;
            }
            if (p1t1.purplelaunched == true)
            {
                thread12.Abort();
                thread4.Start();
                purple1t1.wait();
                purple1t1.wait();
                purple1t1.coltype = Colortype.purple;
                purple1t1.blackclicked2 = true;
            }
            if (p1t1.orangelaunched == true)
            {
                thread16.Abort();
                thread15.Start();
                orange1t1.wait();
                orange1t1.wait();
                orange1t1.wait();
                orange1t1.coltype = Colortype.orange;
                orange1t1.blackclicked2 = true;
            }
            if (p1t1.yellowlaunched == true)
            {
                thread19.Abort();
                thread18.Start();
                yellow1t1.wait();
                yellow1t1.wait();
                yellow1t1.wait();
                yellow1t1.wait();
                yellow1t1.coltype = Colortype.yellow;
                yellow1t1.blackclicked2 = true;
            }*/


            p2t1.blackclicked2 = true;
            p2t1.waitingwagon();
            p2t1.blackclicked2 = false;
            if (p2t2.orangelaunched == true)
            {
                orange2t1.blackclicked2 = true;
                orange2t1.waitingwagon();
                orange2t1.blackclicked2 = false;
            }
            if (p2t2.yellowlaunched == true)
            {
                yellow2t1.blackclicked2 = true;
                yellow2t1.waitingwagon();
                yellow2t1.blackclicked2 = false;
            }
            if (p2t2.greenlaunched == true)
            {
                green2t1.blackclicked2 = true;
                green2t1.waitingwagon();
                green2t1.blackclicked2 = false;
            }
            if (p2t2.purplelaunched == true)
            {
                purple2t1.blackclicked2 = true;
                purple2t1.waitingwagon();
                purple2t1.blackclicked2 = false;
            }

            thread8.Abort();
            thread2.Start();
            p2t2.coltype = Colortype.red;
            p2t2.blackclicked2 = true;
            if (p2t2.greenlaunched == true)
            {
                thread11.Abort();
                thread10.Start();
                green2t2.wait();
                green2t2.coltype = Colortype.green;
                green2t2.blackclicked2 = true;
            }
            if (p2t2.purplelaunched == true)
            {
                thread14.Abort();
                thread13.Start();
                purple2t2.wait();
                purple2t2.wait();
                purple2t2.coltype = Colortype.purple;
                purple2t2.blackclicked2 = true;
            }
            if (p2t2.orangelaunched == true)
            {
                thread17.Abort();
                thread5.Start();
                orange2t2.wait();
                orange2t2.wait();
                orange2t2.wait();
                orange2t2.coltype = Colortype.orange;
                orange2t2.blackclicked2 = true;
            }
            if (p2t2.yellowlaunched == true)
            {
                thread20.Abort();
                thread6.Start();
                yellow2t2.wait();
                yellow2t2.wait();
                yellow2t2.wait();
                yellow2t2.wait();
                yellow2t2.coltype = Colortype.yellow;
                yellow2t2.blackclicked2 = true;
            }
        }

        private Panel panel2;

        private void button8_Click(object sender, EventArgs e)
        {
            p1t1.ontrack = false;
            p2t2.ontrack = false;
            p1t1.blackclicked = true;
            p1t1.waitingwagon();
            p1t1.blackclicked = false;
            p1t1.locwag = typetrain.tertiary;
            if(p1t1.greenlaunched == true)
            {
                green1t1.blackclicked = true;
                green1t1.waitingwagon();
                green1t1.blackclicked = false;
                green1t1.locwag = typetrain.tertiary;
            }
            if (p1t1.purplelaunched == true)
            {
                purple1t1.blackclicked = true;
                purple1t1.waitingwagon();
                purple1t1.blackclicked = false;
                purple1t1.locwag = typetrain.tertiary;
            }
            if (p1t1.orangelaunched == true)
            {
                orange1t1.blackclicked = true;
                orange1t1.waitingwagon();
                orange1t1.blackclicked = false;
            }
            if (p1t1.yellowlaunched == true)
            {
                yellow1t1.blackclicked = true;
                yellow1t1.waitingwagon();
                yellow1t1.blackclicked = false;
            }

            thread1.Abort();
            thread7.Start();
            p1t2.blackclicked = true;
            if(p1t1.greenlaunched == true)
            {
                thread3.Abort();
                thread9.Start();
                green1t2.wait();
                green1t2.coltype = Colortype.green;
                green1t2.blackclicked = true;
            }
            if (p1t1.purplelaunched == true)
            {
                thread4.Abort();
                thread12.Start();
                purple1t2.wait();
                purple1t2.wait();
                purple1t2.coltype = Colortype.purple;
                purple1t2.blackclicked = true;
            }
            if (p1t1.orangelaunched == true)
            {
                thread15.Abort();
                thread16.Start();
                orange1t2.wait();
                orange1t2.wait();
                orange1t2.wait();
                orange1t2.coltype = Colortype.orange;
                orange1t2.blackclicked = true;
            }
            if (p1t1.yellowlaunched == true)
            {
                thread18.Abort();
                thread19.Start();
                yellow1t2.wait();
                yellow1t2.wait();
                yellow1t2.wait();
                yellow1t2.wait();
                yellow1t2.coltype = Colortype.yellow;
                yellow1t2.blackclicked = true;
            }


            p2t2.blackclicked = true;
            p2t2.waitingwagon();
            p2t2.blackclicked = false;
            p2t2.locwag = typetrain.tertiary;
            if (p2t2.orangelaunched == true)
            {
                orange2t2.blackclicked = true;
                orange2t2.waitingwagon();
                orange2t2.blackclicked = false;
                orange2t2.locwag = typetrain.tertiary;
            }
            if (p2t2.yellowlaunched == true)
            {
                yellow2t2.blackclicked = true;
                yellow2t2.waitingwagon();
                yellow2t2.blackclicked = false;
                yellow2t2.locwag = typetrain.tertiary;
            }
            if (p2t2.greenlaunched == true)
            {
                green2t2.blackclicked = true;
                green2t2.waitingwagon();
                green2t2.blackclicked = false;
            }
            if (p2t2.purplelaunched == true)
            {
                purple2t2.blackclicked = true;
                purple2t2.waitingwagon();
                purple2t2.blackclicked = false;
            }

            thread2.Abort();
            thread8.Start();
            p2t1.coltype = Colortype.red;
            p2t1.blackclicked = true;
            if (p2t2.greenlaunched == true)
            {
                thread11.Abort();
                thread10.Start();
                green2t1.wait();
                green2t1.coltype = Colortype.green;
                green2t1.blackclicked = true;
            }
            if (p2t2.purplelaunched == true)
            {
                thread14.Abort();
                thread13.Start();
                purple2t1.wait();
                purple2t1.wait();
                purple2t1.coltype = Colortype.purple;
                purple2t1.blackclicked = true;
            }
            if (p2t2.orangelaunched == true)
            {
                thread5.Abort();
                thread17.Start();
                orange2t1.wait();
                orange2t1.wait();
                orange2t1.wait();
                orange2t1.coltype = Colortype.orange;
                orange2t1.blackclicked = true;
            }
            if (p2t2.yellowlaunched == true)
            {
                thread6.Abort();
                thread20.Start();
                yellow2t1.wait();
                yellow2t1.wait();
                yellow2t1.wait();
                yellow2t1.wait();
                yellow2t1.coltype = Colortype.yellow;
                yellow2t1.blackclicked = true;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (p2t2.ontrack == true)
            {
                thread5.Start();
                p2t2.orangeclicked = true;
                p2t2.waitingwagon();
                p2t2.orangeclicked = false;
                p2t2.orangelaunched = true;
                orange2t2.orangeclicked = true;
            }
            else
            {
                thread16.Start();
                p1t2.orangeclicked = true;
                p1t2.waitingwagon();
                p1t2.orangeclicked = false;
                p1t2.orangelaunched = true;
                orange1t2.orangeclicked = true;
            }
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (p1t1.ontrack == true)
            {
                thread3.Start();
                p1t1.greenclicked = true;
                p1t1.waitingwagon();
                p1t1.greenclicked = false;
                p1t1.greenlaunched = true;
                green1t1.greenclicked = true;
            }
            else
            {
                thread10.Start();
                p2t1.greenclicked = true;
                p2t1.waitingwagon();
                p2t1.greenclicked = false;
                p2t1.greenlaunched = true;
                green2t1.greenclicked = true;
            }
        }

        private void pnl1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (p1t1.ontrack == true)
            {
                thread4.Start();
                p1t1.purpleclicked = true;
                p1t1.waitingwagon();
                p1t1.purpleclicked = false;
                p1t1.purplelaunched = true;
                purple1t1.purpleclicked = true;
            }
            else
            {
                thread13.Start();
                p2t1.purpleclicked = true;
                p2t1.waitingwagon();
                p2t1.purpleclicked = false;
                p2t1.purplelaunched = true;
                purple2t1.purpleclicked = true;
            }
        }

        private Panel panel6;
        private Button button7;
        private Panel panel4;
        public Panel panel1;
        private Button button8;
    }// end class form1
}
